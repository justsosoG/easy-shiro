package org.njgzr.security.interfaces;
/**
*@author Mr Gu[admin@njgzr.org]
*@version Dec 9, 2019 , 3:26:25 PM
*/
public interface LoginResultService {
	
	/**
	 * 登陆成功后的回调<br>
	 * Login successful callback interface
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 11, 2019 , 3:35:22 PM
	 * @param userId userId
	 * @param loginName loginName
	 * @param terminal terminal
	 * @param addr addr
	 * @param ip ip
	 * @param os os
	 * @param browser browser
	 */
	void loginSuccess(Long userId,String loginName,String terminal,String addr,String ip,String os,String browser);
	
	/**
	 * 登录失败后的回调<br>
	 * Login fail callback interface
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 11, 2019 , 3:35:50 PM
	 * @param loginName loginName
	 * @param e e
	 */
	void loginFail(String loginName,Exception e);
	
	/**
	 * 登出回调<br>
	 * Logout callback interface
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 13, 2019 , 12:42:34 PM
	 * @param userId userId
	 * @param loginName loginName
	 */
	void logout(Long userId,String loginName);
	
}
