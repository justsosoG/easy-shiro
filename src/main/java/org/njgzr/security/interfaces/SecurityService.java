package org.njgzr.security.interfaces;

import org.njgzr.security.base.AuthorizedUser;
import org.njgzr.security.base.Password;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 9, 2019 , 3:48:24 PM
 * Description
 */
public interface SecurityService {
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 11, 2019 , 3:36:10 PM
	 * @param principal principal
	 * @return AuthorizedUser AuthorizedUser
	 */
	AuthorizedUser findByPrincipal(Object principal);
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 11, 2019 , 3:36:23 PM
	 * @param user user
	 * @return Password Password
	 */
	Password findPassword(AuthorizedUser user);
	
}
