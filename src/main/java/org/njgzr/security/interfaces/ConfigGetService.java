package org.njgzr.security.interfaces;

import java.util.List;

import org.springframework.data.redis.core.StringRedisTemplate;

/**
*@author Mr Gu[admin@njgzr.org]
*@version Dec 10, 2019 , 4:18:28 PM
*/
public interface ConfigGetService {
	
	/**
	 * 同一个账号最多同时登陆的个数,返回null则默认为2<br>
	 * Maximum number of people logged in at the same time with the same account
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 11, 2019 , 3:32:11 PM
	 * @return Long
	 */
	Long maxSessionCount();
	
	/**
	 * 不需要登录就能访问的接口<br>
	 * Interface address that does not need to be verified
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 11, 2019 , 3:33:04 PM
	 * @return String
	 */
	List<String> anons();
	
	/**
	 * redis链接，如果返回null，则自动使用内存进行缓存<br>
	 * redis Connect config
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 11, 2019 , 3:33:54 PM
	 * @return StringRedisTemplate
	 */
	StringRedisTemplate getStringRedisTemplate();
	
	/**
	 * 系统标识<br>
	 * System identification
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 11, 2019 , 3:34:34 PM
	 * @return String
	 */
	String getAppId();
	
	/**
	 * web应用的令牌有效时间，返回null则默认为30分钟<br>
	 * webExpireTime default 30 minutes
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 12, 2019 , 10:34:04 AM
	 * @return Long
	 */
	Long webExpireTime();
	
	/**
	 * 手机应用的令牌有效时间，返回null则默认为1个月<br>
	 * appExpireTime default 1 month
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 12, 2019 , 10:34:39 AM
	 * @return Long
	 */
	Long appExpireTime();
	
	/**
	 * pc应用的令牌有效时间，返回null则默认为1天<br>
	 * pcExpireTime default 1 day
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 12, 2019 , 10:35:16 AM
	 * @return Long
	 */
	Long pcExpireTime();
	
	/**
	 * 登录请求中用户名的字段，返回null则默认为username<br>
	 * Login username parameters，if return null， default 'username'
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 13, 2019 , 10:55:02 AM
	 * @return string
	 */
	String loginUserNameParam();
	
	/**
	 * 登录请求中密码的字段，返回null则默认为password<br>
	 * Login password parameters，if return null， default 'password'
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 13, 2019 , 10:56:22 AM
	 * @return string
	 */
	String loginPasswordParam();
	
	/**
	 * 登录请求中验证码的字段，返回null则默认为captchaCode
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 16, 2019 , 9:50:48 AM
	 * @return String
	 */
	String captchaParam();
	
	/**
	 * 请求头中的令牌字段，返回null则默认为 token<br>
	 * The custom token field in the request header, if return null， default 'token'
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 13, 2019 , 10:58:38 AM
	 * @return string
	 */
	String headerToken();
	
	/**
	 * 登录接口的地址，返回null则默认是 /login<br>
	 * set login url，if return null， default '/login'
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 13, 2019 , 11:15:10 AM
	 * @return string
	 */
	String loginUrl();
	
	/**
	 * 是否开启验证码<br>
	 * enableCaptcha
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 16, 2019 , 10:14:18 AM
	 * @return boolean
	 */
	boolean enableCaptcha();
	
	/**
	 * 最大的错误次数，如果超过该数字或者返回0，系统将校验验证码<br>
	 * the max login fail count,if more than this count or set 0 , we will check captcha
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 16, 2019 , 11:39:48 AM
	 * @return int
	 */
	int maxClfCount();
	
	/**
	 * 验证码的长度，最长6位，最短4位，如为算术验证码，则默认是两个数字的算术<br>
	 * captcha lenth min 4 , max 6
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 16, 2019 , 5:45:41 PM
	 * @return int
	 */
	int captchaLenth();
	
	/**
	 * 验证码图片的大小，格式为  宽,高 ，逗号为英文状态的逗号，返回null则默认 110,40<br>
	 * captchaSize like width,height,default 110,40
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 16, 2019 , 5:52:50 PM
	 * @return String
	 */
	String captchaSize();
	
	/**
	 * 验证码类型，目前可选的为 specCaptcha,gifCaptcha,chineseCaptcha,chineseGifCaptcha,arithmeticCaptcha，<br>
	 * 如果返回null，则默认随机，中文验证码存在问题，暂时不推荐使用<br>
	 * 可选值参考org.njgzr.security.enums.CaptchaType<br>
	 * captchaType :[specCaptcha,gifCaptcha,chineseCaptcha,chineseGifCaptcha,arithmeticCaptcha],specCaptcha is default
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 17, 2019 , 11:11:44 AM
	 * @return String
	 */
	String captchaType();
	
}
