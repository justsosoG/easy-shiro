package org.njgzr.security.base.token;

import org.apache.shiro.authc.UsernamePasswordToken;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 16, 2019 , 9:42:40 AM
*/
@Getter @Setter @ToString
public class UsernamePasswordWithCaptchaToken extends UsernamePasswordToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7807514094484431068L;
	
	private String captchaCode = null;
	
	public UsernamePasswordWithCaptchaToken() {
		super();
	}
	
	public UsernamePasswordWithCaptchaToken(String username, String password) {
		super(username, password);
	}
	
	public UsernamePasswordWithCaptchaToken(String username, String password,String captchaCode) {
		super(username, password);
		setCaptchaCode(captchaCode);
	}
	
}
