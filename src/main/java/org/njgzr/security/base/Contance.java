package org.njgzr.security.base;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 11, 2019 , 5:28:34 PM
 * Description
 */
public class Contance {
	
	public static final String HEADER = "token";
	
	public static final String TERMINAL = "terminal";
	
	/**
	 * token gracePeriod
	 */
	public static final long gracePeriod = 60*1000L;
	
	/**
	 * webExpireTime
	 */
	public static final long webExpireTime =      	   30 * 60 * 1000L;
	
	/**
	 * appExpireTime
	 */
	public static final long appExpireTime = 30 * 24 * 60 * 60 * 1000L;
	
	/**
	 * pcExpireTime
	 */
	public static final long pcExpireTime = 	  24 * 60 * 60 * 1000L;
	
	
}
