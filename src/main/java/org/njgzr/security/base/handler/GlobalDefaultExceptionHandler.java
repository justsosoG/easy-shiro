package org.njgzr.security.base.handler;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.UnauthorizedException;
import org.njgzr.security.base.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 13, 2019 , 4:29:32 PM
*/
@ControllerAdvice
public class GlobalDefaultExceptionHandler {
	
	@ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public Result defaultExceptionHandler(HttpServletRequest req,Exception e){
		return Result.fail(e.getMessage());
    }
	
}
