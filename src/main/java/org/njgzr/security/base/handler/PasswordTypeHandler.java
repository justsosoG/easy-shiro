//package org.njgzr.security.base.handler;
//
//import java.sql.CallableStatement;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import org.apache.ibatis.type.BaseTypeHandler;
//import org.apache.ibatis.type.JdbcType;
//import org.njgzr.security.base.Password;
//
//public class PasswordTypeHandler extends BaseTypeHandler<Password> {
//	/** 
//	 * <p>Title setNonNullParameter</p>  
//	 * <p>Description </p>  
//	 * @param ps
//	 * @param i
//	 * @param parameter
//	 * @param jdbcType
//	 * @throws SQLException 
//	 * @see org.apache.ibatis.type.BaseTypeHandler#setNonNullParameter(java.sql.PreparedStatement, int, java.lang.Object, org.apache.ibatis.type.JdbcType)  
//	 */
//	@Override
//	public void setNonNullParameter(PreparedStatement ps, int i, Password parameter, JdbcType jdbcType)
//			throws SQLException {		
//		ps.setString(i, parameter.toString());
//	}
//	
//	private Password covert(String valStrs) {
//		
//		return Password.parse(valStrs);
//	}
//	/** 
//	 * <p>Title getNullableResult</p>  
//	 * <p>Description </p>  
//	 * @param rs
//	 * @param columnName
//	 * @return
//	 * @throws SQLException 
//	 * @see org.apache.ibatis.type.BaseTypeHandler#getNullableResult(java.sql.ResultSet, java.lang.String)  
//	 */
//	@Override
//	public Password getNullableResult(ResultSet rs, String columnName) throws SQLException {
//		return covert(rs.getString(columnName));
//	}
//
//	/** 
//	 * <p>Title getNullableResult</p>  
//	 * <p>Description </p>  
//	 * @param rs
//	 * @param columnIndex
//	 * @return
//	 * @throws SQLException 
//	 * @see org.apache.ibatis.type.BaseTypeHandler#getNullableResult(java.sql.ResultSet, int)  
//	 */
//	@Override
//	public Password getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
//		return covert(rs.getString(columnIndex));
//	}
//
//	/** 
//	 * <p>Title getNullableResult</p>  
//	 * <p>Description </p>  
//	 * @param cs
//	 * @param columnIndex
//	 * @return
//	 * @throws SQLException 
//	 * @see org.apache.ibatis.type.BaseTypeHandler#getNullableResult(java.sql.CallableStatement, int)  
//	 */
//	@Override
//	public Password getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
//		return covert(cs.getString(columnIndex));
//	}
//}
