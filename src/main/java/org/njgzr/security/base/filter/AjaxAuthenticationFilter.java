package org.njgzr.security.base.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.njgzr.security.base.AuthorizedUser;
import org.njgzr.security.base.CaptchaMode;
import org.njgzr.security.base.Contance;
import org.njgzr.security.base.Result;
import org.njgzr.security.base.token.UsernamePasswordWithCaptchaToken;
import org.njgzr.security.cache.LoginCacheService;
import org.njgzr.security.code.CaptchaRender;
import org.njgzr.security.code.CodeService;
import org.njgzr.security.event.LoginFailEvent;
import org.njgzr.security.event.LoginSuccessEvent;
import org.njgzr.security.interfaces.ConfigGetService;
import org.njgzr.security.service.JwtService;
import org.njgzr.security.utils.HttpResponseHelper;
import org.springframework.context.ApplicationContext;

import com.alibaba.fastjson.JSON;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Mr Gu [admin@njgzr.org]
 * @version Dec 11, 2019 , 5:31:14 PM
 * Description
 */
@Getter
@Slf4j
public class AjaxAuthenticationFilter extends AuthenticatingFilter  {
	
	public static final String DEFAULT_USERNAME_PARAM = "username";
    public static final String DEFAULT_PASSWORD_PARAM = "password";
    public static final String DEFAULT_TERMINAL_TYPE_PARAM = "teminal";
    public static final String DEFAULT_CAPTCHA_PARAM = "captchaCode";
    
    private boolean enableCaptcha = false;
	
	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		if (isLoginRequest(request, response)) {
			if (isLoginSubmission(request, response)) {//只支持POST方式的Login请求
				UsernamePasswordWithCaptchaToken token = createToken(request, response);
				return executeLogin(request, response,token);
			}else {
				log.debug("Unsupport GET Login .");
				new HttpResponseHelper(response).responseJson(Result.fail("非法的登录请求!").toJson());
				return false;
			}
		}else {
			 new HttpResponseHelper(response).responseJson(Result.fail("您尚未登录或登录时间过长,请重新登录!").toJson());
	         return false;
		}
	}
	
	protected boolean isLoginSubmission(ServletRequest request, ServletResponse response) {
        return (request instanceof HttpServletRequest) && WebUtils.toHttp(request).getMethod().equalsIgnoreCase(POST_METHOD);
    }
	
	protected UsernamePasswordWithCaptchaToken createToken(ServletRequest request, ServletResponse response) throws Exception {
    	String contentType = request.getContentType();
    	String lnpString = configGetService.loginUserNameParam();
    	String lppString = configGetService.loginPasswordParam();
    	String cpString = configGetService.captchaParam();
    	
    	String upString = StringUtils.isBlank(lnpString)?DEFAULT_USERNAME_PARAM:lnpString;
    	String ppString = StringUtils.isBlank(lppString)?DEFAULT_PASSWORD_PARAM:lppString;
    	String cString = StringUtils.isBlank(cpString)?DEFAULT_CAPTCHA_PARAM:cpString;
    	
    	if(contentType.contains("application/x-www-form-urlencoded")){
    		
			String username = WebUtils.getCleanParam(request, upString);
			String password = WebUtils.getCleanParam(request, ppString);
			String captchaCode = WebUtils.getCleanParam(request, cString);
			return new UsernamePasswordWithCaptchaToken(username,password,captchaCode);
    	}else if(contentType.contains("application/json")){
			String reqContent = IOUtils.toString(request.getInputStream(),"utf-8");
			String username = JSON.parseObject(reqContent).getString(upString);
			String password = JSON.parseObject(reqContent).getString(ppString);
			String captchaCode = JSON.parseObject(reqContent).getString(cString);
			return new UsernamePasswordWithCaptchaToken(username,password,captchaCode);
    	}
    	throw new RuntimeException("不支持的请求方式，Content-type:"+contentType);
    }

	protected boolean executeLogin(ServletRequest request, ServletResponse response,UsernamePasswordWithCaptchaToken token) throws Exception {
		if(enableCaptcha&&!validateCaptcha(request, response, token)){
			Result result = Result.fail("","验证码错误");
			result.setData(new CaptchaMode(true));
			new HttpResponseHelper(response).responseJson(result.toJson());
			return false;
		}
		if (token == null) {
			return onLoginFailure(token, new AccountException("验证异常，请重新登陆！"), request, response);
        }
        try {
            Subject subject = getSubject(request, response);
            subject.login(token);
            return onLoginSuccess(token, subject, request, response);
        } catch (AuthenticationException e) {
            return onLoginFailure(token, e, request, response);
        }
	}
	
	protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request,ServletResponse response) throws Exception {
		AuthorizedUser user = (AuthorizedUser)subject.getPrincipal();
		int terminal = NumberUtils.toInt(WebUtils.toHttp(request).getHeader(Contance.TERMINAL), 1);
		String jwtToken = jwtService.createToken(user.getLoginName(),terminal);
		applicationContext.publishEvent(new LoginSuccessEvent(user, WebUtils.toHttp(request),jwtToken,false));
		HttpResponseHelper respHelper = new HttpResponseHelper(response);
		respHelper.resposeJwtToken(jwtToken,configGetService.headerToken());
		respHelper.responseJson(Result.success(user).toJson());
		return false;
	}
	
	@Override
	protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request,
			ServletResponse response) {
		
		Result result = Result.fail("","用户名或密码输入错误.");
		
		if(e instanceof LockedAccountException)
			result.setDesc("账号已被锁定.");
	    else if(e instanceof DisabledAccountException)
	    	result.setDesc("账号已过期失效.");
	    else if(e instanceof AccountException)
	    	result.setDesc(e.getMessage());
		
		if(enableCaptcha)
			result.setData(new CaptchaMode(codeService.isCaptchaEnabled(token.getPrincipal().toString())));
	    result.setCode(HttpServletResponse.SC_UNAUTHORIZED);
	    
		applicationContext.publishEvent(new LoginFailEvent(token.getPrincipal()+"", e));
		HttpResponseHelper respHelper = new HttpResponseHelper(response);
		respHelper.responseJson(result.toJson());
		return false;
	}
	
	public AjaxAuthenticationFilter(JwtService jwtService,LoginCacheService loginCache,
			ApplicationContext applicationContext,ConfigGetService configGetService,CodeService codeService) {
		super();
		this.jwtService = jwtService;
		this.loginCache = loginCache;
		this.applicationContext = applicationContext;
		this.configGetService = configGetService;
		this.codeService = codeService;
		setEnableCaptcha(configGetService.enableCaptcha());
	}
    
	/**
	 * @param enableCaptcha the enableCaptcha to set
	 */
	public void setEnableCaptcha(boolean enableCaptcha) {
		this.enableCaptcha = enableCaptcha;
	}
	
	private boolean validateCaptcha(ServletRequest request, ServletResponse response,UsernamePasswordWithCaptchaToken token){
		if(!codeService.isCaptchaEnabled(token.getUsername())){
        	return true;
        }
		CaptchaRender render = new CaptchaRender();
		return render.validate2(token.getCaptchaCode(),WebUtils.toHttp(request));
	}
	
	private JwtService jwtService;
	
	private LoginCacheService loginCache;
	
	private ApplicationContext applicationContext;
	
	private ConfigGetService configGetService;
	
	private CodeService codeService;
	
}
