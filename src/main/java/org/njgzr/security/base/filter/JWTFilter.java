package org.njgzr.security.base.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.njgzr.security.base.AuthorizedUser;
import org.njgzr.security.base.Contance;
import org.njgzr.security.base.Result;
import org.njgzr.security.base.token.JWTToken;
import org.njgzr.security.cache.LoginCacheService;
import org.njgzr.security.event.LoginSuccessEvent;
import org.njgzr.security.interfaces.ConfigGetService;
import org.njgzr.security.service.JwtService;
import org.njgzr.security.utils.HttpResponseHelper;
import org.njgzr.security.utils.JWTUtil;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JWTFilter extends BasicHttpAuthenticationFilter {

    @Override
    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
    	String authorization = getAuthzHeader(request);
        return authorization != null;
    }
    
    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
    	 AuthenticationToken token = createToken(request, response);
         if (token == null) {
         	return onLoginFailure(token, new ExpiredCredentialsException("身份验证过期，请重新登录"), request, response);
         }
         
         String tokenString = ((JWTToken)token).getVal();
         
         if(JWTUtil.isTokenExpired(tokenString)) {
          	return onLoginFailure(token, new ExpiredCredentialsException("用户会话超时"), request, response);
         }
         
         String sessionId = DigestUtils.md5Hex(tokenString);
         if(!loginCache.existSession(sessionId)) {
        	 return onLoginFailure(token, new ExpiredCredentialsException("该账号在其他地方登陆，您已被强制退出。"), request, response);
         }
         
         try {
             Subject subject = getSubject(request, response);
             subject.login(token);
             return onLoginSuccess(token, subject, request, response);
         } catch (AuthenticationException e) {
        	 return onLoginFailure(token, e, request, response);
         }
    }
	
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
    	 return false;
    }
    
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
    	String htString = configGetService.headerToken();
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("Access-control-Allow-Origin", httpServletRequest.getHeader("Origin"));
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", httpServletRequest.getHeader("Access-Control-Request-Headers"));
        httpServletResponse.setHeader("Access-Control-Expose-Headers", StringUtils.isBlank(htString)?Contance.HEADER:htString);
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return false;
        }
        return super.preHandle(request, response);
    }
    
    
	protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request,ServletResponse response) throws Exception {
		if (token instanceof JWTToken) {
			JWTToken jwtToken = (JWTToken) token;
			AuthorizedUser user = (AuthorizedUser) subject.getPrincipal();
			log.debug("user request success:"+user.getLoginName());
			
			boolean shouldRefresh = jwtService.shouldRefreshToken(jwtToken.getVal());
			if (shouldRefresh) {
				log.debug("user token refresh:"+user.getLoginName());
				HttpResponseHelper respHelper = new HttpResponseHelper(response);
				respHelper.resposeJwtToken(jwtService.refreshToken(jwtToken.getVal(),user.getLoginName()),configGetService.headerToken());
				applicationContext.publishEvent(new LoginSuccessEvent(user,WebUtils.toHttp(request),jwtToken.getVal(),true));
			}
		}
		return true;
	}
    
	@Override
	protected AuthenticationToken createToken(ServletRequest servletRequest, ServletResponse servletResponse) {
		String jwtToken = getTokenFromRequest(servletRequest);
		if (StringUtils.isNotBlank(jwtToken) )
			return new JWTToken(jwtToken);
		return null;
	}

	private String getTokenFromRequest(ServletRequest servletRequest) {
		String jwtToken = getAuthzHeader(servletRequest);
		return jwtToken;
	}
	
	protected String getAuthzHeader(ServletRequest request) {
		HttpServletRequest httpRequest = WebUtils.toHttp(request);
		String htString = configGetService.headerToken();
		String name = StringUtils.isBlank(htString)?Contance.HEADER:htString;
		String cToken = getAuthzCookie(httpRequest, name);
		return StringUtils.isBlank(cToken)?httpRequest.getHeader(name):cToken;
	}
	
	/**
	 * 优先从cookie中获取秘钥，因为cookie由服务端写入，所以如果浏览器未禁用cookie，则接口调用时可以不添加header
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:21:58 PM
	 * @param httpRequest httpRequest
	 * @param name name
	 * @return String
	 */
	protected String getAuthzCookie(HttpServletRequest httpRequest,String name) {
		Cookie[] cs = httpRequest.getCookies();
		if(cs!=null) {
			for (Cookie cookie : cs) {
				String cname = cookie.getName();
				if(StringUtils.isNotBlank(cname) && cname.equals(name)) {
					log.info("read from cookie");
					return cookie.getValue();
				}
			}
		}
		return null;
	}
	
	@Override
	protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request,
			ServletResponse response) {
		String keyString = JWTUtil.getKey(getTokenFromRequest(request));
		loginCache.removeSession(keyString);
		Result result = Result.fail(HttpServletResponse.SC_UNAUTHORIZED,e.getMessage());
		new HttpResponseHelper(response).responseJson(result.toJson());
		return false;
	}

	public JWTFilter(JwtService jwtService,LoginCacheService loginCache,
			ApplicationContext applicationContext,ConfigGetService configGetService) {
		super();
		this.jwtService = jwtService;
		this.loginCache = loginCache;
		this.applicationContext = applicationContext;
		this.configGetService = configGetService;
	}
    
	private JwtService jwtService;
	
	private LoginCacheService loginCache;
	
	private ApplicationContext applicationContext;
	
	private ConfigGetService configGetService;
	
}
