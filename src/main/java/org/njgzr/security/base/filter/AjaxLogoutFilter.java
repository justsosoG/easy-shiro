package org.njgzr.security.base.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.apache.shiro.web.util.WebUtils;
import org.njgzr.security.base.AuthorizedUser;
import org.njgzr.security.base.Contance;
import org.njgzr.security.base.Result;
import org.njgzr.security.cache.LoginCacheService;
import org.njgzr.security.event.LogoutEvent;
import org.njgzr.security.interfaces.ConfigGetService;
import org.njgzr.security.service.JwtService;
import org.njgzr.security.utils.HttpResponseHelper;
import org.njgzr.security.utils.SecurityUtil;
import org.springframework.context.ApplicationContext;

import lombok.extern.slf4j.Slf4j;

/**
*@author Mr Gu[admin@njgzr.org]
*@version Dec 13, 2019 , 11:44:56 AM
*/
@Slf4j
public class AjaxLogoutFilter extends LogoutFilter {

	@Override
	protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
		AuthorizedUser user = SecurityUtil.getCurrentUser();
		Subject subject = getSubject(request, response);
		String jwtToken = getAuthzHeader(request);
		jwtService.clearToken(jwtToken);
		loginCache.removeSession(DigestUtils.md5Hex(jwtToken));
		try {
            subject.logout();
        } catch (SessionException ise) {
            log.debug("Encountered session exception during logout.  This can generally safely be ignored.", ise);
        }
		new HttpResponseHelper(response).responseJson(Result.success().toJson());
		applicationContext.publishEvent(new LogoutEvent(user));
		return false;
	}
	
	protected String getAuthzHeader(ServletRequest request) {
		HttpServletRequest httpRequest = WebUtils.toHttp(request);
		String htString = configGetService.headerToken();
		return httpRequest.getHeader(StringUtils.isBlank(htString)?Contance.HEADER:htString);
	}
	public AjaxLogoutFilter(JwtService jwtService,LoginCacheService loginCache,
			ApplicationContext applicationContext,ConfigGetService configGetService) {
		super();
		this.jwtService = jwtService;
		this.loginCache = loginCache;
		this.applicationContext = applicationContext;
		this.configGetService = configGetService;
	}
	
	private JwtService jwtService;
	
	private LoginCacheService loginCache;
	
	private ApplicationContext applicationContext;
	
	private ConfigGetService configGetService;
	
	
}
