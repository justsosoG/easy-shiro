package org.njgzr.security.base.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.njgzr.security.code.CaptchaRender;
import org.njgzr.security.interfaces.ConfigGetService;
import org.springframework.web.filter.OncePerRequestFilter;

/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 16, 2019 , 10:33:52 AM
*/
public class CodeFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		CaptchaRender render = new CaptchaRender();
		render.render(request,response,configGetService);
	}
	
	public CodeFilter(ConfigGetService configGetService) {
		this.configGetService = configGetService;
	}
	
	private ConfigGetService configGetService;
	
}
