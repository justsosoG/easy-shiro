package org.njgzr.security.base;

import com.alibaba.fastjson.JSON;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CaptchaMode {

	private boolean captchaEnabled = false;
	
	public String toJson(){
		return JSON.toJSONString(this);
	}
	
}
