package org.njgzr.security.base.realm;

import javax.annotation.PostConstruct;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.njgzr.security.base.AuthorizedUser;
import org.njgzr.security.base.Encodes;
import org.njgzr.security.base.Password;
import org.njgzr.security.base.token.UsernamePasswordWithCaptchaToken;
import org.njgzr.security.credential.MultiCredentialsMatcher;
import org.njgzr.security.interfaces.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 9, 2019 , 3:46:49 PM
*/
@Service
public class DbRealm extends AuthorizingRealm {
	
	@Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof UsernamePasswordWithCaptchaToken;
    }
	
	/**
	 * 
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
	
		AuthorizedUser user = this.securityService.findByPrincipal(authcToken.getPrincipal());
		
		if (user != null) {
			if(user.isLocked())
				throw new LockedAccountException();

			if(user.isDisable())
				throw new DisabledAccountException();			
			
			Password password =  this.securityService.findPassword(user);
			ByteSource salt = password.getSalt()==null?null: ByteSource.Util.bytes(Encodes.decodeHex(password.getSalt()));
			return new SimpleAuthenticationInfo(user,password.getValue(),salt, getName());
		} else {
			return null;
		}
	}

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		AuthorizedUser user = (AuthorizedUser)principals.getPrimaryPrincipal();
		
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		if(null != user.getStringRoles()) {
			info.addRoles(user.getStringRoles());
		}
		if(null != user.getStringPermissions()) {
			info.addStringPermissions(user.getStringPermissions());
		}
		return info;
	}

	@PostConstruct
	public void initCredentialsMatcher() {
		HashedCredentialsMatcher dafaultMatcher = new HashedCredentialsMatcher(Password.HASH_ALGORITHM_SHA1);
		dafaultMatcher.setHashIterations(Password.HASH_INTERATIONS);

		HashedCredentialsMatcher md5Matcher = new HashedCredentialsMatcher(Password.HASH_ALGORITHM_MD5);
		
		
		this.setCredentialsMatcher(new MultiCredentialsMatcher(dafaultMatcher,md5Matcher));
	}

	@Autowired(required=false)
	private SecurityService securityService;
	
}
