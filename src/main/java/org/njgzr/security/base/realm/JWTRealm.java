package org.njgzr.security.base.realm;

import javax.annotation.PostConstruct;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.njgzr.security.base.AuthorizedUser;
import org.njgzr.security.base.token.JWTToken;
import org.njgzr.security.credential.JWTCredentialsMatcher;
import org.njgzr.security.interfaces.SecurityService;
import org.njgzr.security.service.JwtService;
import org.njgzr.security.utils.JWTUtil;
import org.njgzr.security.utils.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JWTRealm extends AuthorizingRealm {
	
	@Autowired
	private JwtService jwtService;
	
	@Autowired
	private SecurityService securityService;
	
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    	AuthorizedUser users = SecurityUtil.getCurrentUser();
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        if(null != users.getStringRoles()) {
        	simpleAuthorizationInfo.addRoles(users.getStringRoles());
        }
        if(null != users.getStringPermissions()) {
        	simpleAuthorizationInfo.addStringPermissions(users.getStringPermissions());
        }
        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
    	JWTToken jwtToken = (JWTToken) auth;
        String token = jwtToken.getVal();
        
        AuthorizedUser user = this.securityService.findByPrincipal(JWTUtil.getUsername(token));
        
        if(user == null)
            throw new AuthenticationException("token过期，请重新登录");

        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(user, token, getName());
        return authenticationInfo;
    }
    
    
    @PostConstruct
    private void initCredentialsMatcher() {

        this.setCredentialsMatcher(new JWTCredentialsMatcher(jwtService));
    }
    
}
