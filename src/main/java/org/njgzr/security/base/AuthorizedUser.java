package org.njgzr.security.base;

import java.util.Date;
import java.util.Set;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 9, 2019 , 3:00:04 PM
 * Description User interface certified by security interface
 */
public interface AuthorizedUser {
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:28:11 PM
	 * @return 系统用户的唯一编号，通常为数据库id
	 */
	Long getId();
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:28:50 PM
	 * @return 系统用户的所属部门唯一编号，通常为数据库id
	 */
	Long getOrganizationId();
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:29:08 PM
	 * @return 系统用户的登录名
	 */
	String getLoginName();
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:29:20 PM
	 * @return 系统用户的显示名称
	 */
	String getDisplayName();
	
	String getMobile();
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:29:42 PM
	 * @return 此用于控制用户登录时，是否账号锁定的逻辑
	 */
	boolean isLocked();
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:30:30 PM
	 * @return 此用于控制用户登录时，是否账号不可用的逻辑
	 */
	boolean isDisable();
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:30:46 PM
	 * @return 此用于控制用户登录时，是否账号已过期的逻辑
	 */
	Date getExpireTime();
	
	/**
	 * 此用于接口角色校验
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:27:07 PM
	 * @return 角色
	 */
	Set<String> getStringRoles();
	
	/**
	 * 此用于权限角色校验
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:27:18 PM
	 * @return 权限
	 */
	Set<String> getStringPermissions();
	
	/**
	 * SecurityUtil.getCurrentUser.getUser()，使用该方法可以直接获取到登陆的User对象，无需再从db获取。
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:24:40 PM
	 * @param <T> 泛型
	 * @return user对象
	 */
	<T> T getUser();
	
}

