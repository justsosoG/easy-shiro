package org.njgzr.security.utils;

import org.apache.shiro.SecurityUtils;
import org.njgzr.security.base.AuthorizedUser;


/**
*@author Mr Gu[admin@njgzr.org]
*@version Dec 13, 2019 , 10:30:58 AM
*/
public class SecurityUtil {
	
	public static AuthorizedUser getCurrentUser() {
		return (AuthorizedUser) SecurityUtils.getSubject().getPrincipal();
	}
	
}
