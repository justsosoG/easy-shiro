package org.njgzr.security.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.web.util.WebUtils;
import org.njgzr.security.base.Contance;

import lombok.extern.slf4j.Slf4j;


/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 11, 2019 , 5:35:21 PM
 * Description
 */
@Slf4j
public class HttpResponseHelper {

	private HttpServletResponse resp = null;
	
	public HttpResponseHelper(ServletResponse response){
		resp = WebUtils.toHttp(response);
	}
	
	public void resposeJwtToken(String jwtToken,String htoken) {
		if(jwtToken!=null) {
			htoken = StringUtils.isBlank(htoken)?Contance.HEADER:htoken;
			resp.setHeader(htoken, jwtToken);
			try {
				Cookie cookie = new Cookie(htoken, jwtToken);
				Long tLong = (JWTUtil.getExpiresAt(jwtToken).getTime()-System.currentTimeMillis())/1000;
				cookie.setMaxAge(tLong.intValue());
				resp.addCookie(cookie);
			} catch (Exception e) {
				
			}
    		resp.setHeader("Pragma","no-cache");
			resp.setHeader("Cache-Control","no-cache");
			resp.setHeader("Access-Control-Expose-Headers", htoken);
		}
	}
	public boolean responseJson(String content){
		return responseJson(content,null);
	}
	public boolean responseJson(String content,Integer code){
		if(code!=null)
			try {
				resp.sendError(code);
			} catch (IOException e) {
				// ignore
			}
		resp.setCharacterEncoding("UTF-8");  
		resp.setContentType("application/json; charset=utf-8");  
	    return writeContent(content); 
	}

	private boolean writeContent(String content) {
		try( PrintWriter out = resp.getWriter()) {   
	        out.append(content);  
	        out.flush();
	        return true;
	    } catch (IOException e) {  
	        log.error("response json error:",e);
	        return false;
	    }
	}

}
