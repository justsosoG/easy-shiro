package org.njgzr.security.utils;

import org.njgzr.security.base.Contance;
import org.njgzr.security.interfaces.ConfigGetService;

/**
*@author Mr Gu [admin@njgzr.org]
*@version Jan 2, 2020 , 3:56:02 PM
*/
public class ExpireTime {
	
	public long getExpireTime(ConfigGetService configGetService,int terminal) {
		switch (terminal) {
		case 1:
			return configGetService.webExpireTime()==null?Contance.webExpireTime:configGetService.webExpireTime();
		case 2:
			return configGetService.appExpireTime()==null?Contance.appExpireTime:configGetService.appExpireTime();
		case 3:
			return configGetService.pcExpireTime()==null?Contance.pcExpireTime:configGetService.pcExpireTime();
		case 7:
			return 15*24*60*1000;
		default:
			return configGetService.webExpireTime()==null?Contance.webExpireTime:configGetService.webExpireTime();
	}
}
	
}
