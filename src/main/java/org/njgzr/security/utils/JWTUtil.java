package org.njgzr.security.utils;

import java.util.Calendar;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 11, 2019 , 5:37:50 PM
 * Description
 */
public class JWTUtil {


    public static boolean verify(String token, String username, String salt) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(salt);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim("username", username)
                    .withClaim("key", getKey(token))
                    .build();
            verifier.verify(token);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public static String getUsername(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("username").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }
    
	public static Integer getTerminal(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("teminalType").asInt();
        } catch (JWTDecodeException e) {
            return null;
        }
    }
    
    public static String sign(String username, String salt,String key, long time,int teminalType) {
    	Date issuedTime = new Date();
        Date expireTime = new Date(issuedTime.getTime()+time);
		Algorithm algorithm = Algorithm.HMAC256(salt);
		return JWT.create()
		        .withClaim("username", username)
		        .withClaim("key", key)
		        .withClaim("teminalType", teminalType)
		        .withExpiresAt(expireTime)
                .withIssuedAt(issuedTime)
		        .sign(algorithm);
    }
    
	public static Date getIssuedAt(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getIssuedAt();
        } catch (JWTDecodeException e) {
            return null;
        }
    }
	
	public static Date getExpiresAt(String token) {
		try {
			DecodedJWT jwt = JWT.decode(token);
			return jwt.getExpiresAt();
		} catch (JWTDecodeException e) {
			return null;
		}
	}
	
	public static boolean isTokenExpired(String token) {
        Date now = Calendar.getInstance().getTime();
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getExpiresAt().before(now);
    }
    
	public static String getKey(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("key").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }
	
}
