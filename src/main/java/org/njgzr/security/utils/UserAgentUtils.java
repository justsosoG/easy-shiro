package org.njgzr.security.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

/**
 * 
 * @author Mr Gu [admin@njgzr.org]
 * @version Dec 13, 2019 , 10:41:03 AM
 * Description
 */
public class UserAgentUtils {
	
	public static void getUserAgent(HttpServletRequest request) {
//		String agent = request.getHeader("User-Agent");
//		UserAgent userAgent = UserAgent.parseUserAgentString(agent);
//		Browser browser = userAgent.getBrowser();
//		OperatingSystem operatingSystem = userAgent.getOperatingSystem();
//
//		System.out.println("agent:"+agent);
//		System.out.println("浏览器名:"+browser.getName());
//		System.out.println("浏览器类型:"+browser.getBrowserType());
//		System.out.println("浏览器家族:"+browser.getGroup());
//		System.out.println("浏览器生产厂商:"+browser.getManufacturer());
//		System.out.println("浏览器使用的渲染引擎:"+browser.getRenderingEngine());
//		System.out.println("浏览器版本:"+userAgent.getBrowserVersion());
//		
//		System.out.println("\n操作系统名:"+operatingSystem.getName());
//		System.out.println("访问设备类型:"+operatingSystem.getDeviceType());
//		System.out.println("操作系统家族:"+operatingSystem.getGroup());
//		System.out.println("操作系统生产厂商:"+operatingSystem.getManufacturer());
	}
	
	
	private static String getOsVersion(String userAgent) {
		String osVersion = "";
		if(StringUtils.isBlank(userAgent)) 
			return osVersion;
		String[] strArr = userAgent.substring(userAgent.indexOf("(")+1,
				userAgent.indexOf(")")).split(";");
		if(null == strArr || strArr.length == 0) 
			return osVersion;
		
		osVersion = strArr[1];
		return osVersion;
	}
	
	public static String getOs(String agent) {
		try {
			UserAgent userAgent = UserAgent.parseUserAgentString(agent);
			OperatingSystem operatingSystem = userAgent.getOperatingSystem();
			return operatingSystem.getName()+"-"+getOsVersion(agent);
		} catch (Exception e) {
			return "UNKNOW";
		}
	}
	
	public static String getBrowser(String agent) {
		try {
			UserAgent userAgent = UserAgent.parseUserAgentString(agent);
			Browser browser = userAgent.getBrowser();
			return browser.getName()+"-"+userAgent.getBrowserVersion();
		} catch (Exception e) {
			return "UNKNOW";
		}
	}
	
}
