package org.njgzr.security.utils;

import java.io.File;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.lionsoul.ip2region.DataBlock;
import org.lionsoul.ip2region.DbConfig;
import org.lionsoul.ip2region.DbSearcher;

import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 11, 2019 , 5:35:34 PM
 * Description
 */
@Slf4j
public class IpUtil {
	
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
	    if(StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
	    	int index = ip.indexOf(",");
	        if(index != -1){
	            return ip.substring(0,index);
	        }else{
	            return ip;
	        }
	    }
	    ip = request.getHeader("X-Real-IP");
	    if(StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
	       return ip;
	    }
	    return request.getRemoteAddr();
	}
	
	public static Map<String,String> getCityInfo(HttpServletRequest request) {
		Map<String,String> map = Maps.newHashMap();
		String ip = getIpAddr(request);
		map.put("ip", ip);
		log.debug("IP是："+ip);
		try {
	        DbConfig config = new DbConfig();

	        String dbPath = IpUtil.class.getResource("/ip2region.db").getPath();
	        
            File file = new File(dbPath);
            if (file.exists() == false) {

                String tmpDir = System.getProperties().getProperty("java.io.tmpdir");
                dbPath = tmpDir + "ip.db";
                file = new File(dbPath);
                FileUtils.copyInputStreamToFile(IpUtil.class.getClassLoader().getResourceAsStream("classpath:ip2region.db"), file);

            }

	        DbSearcher searcher = new DbSearcher(config, dbPath);

	        DataBlock block = searcher.btreeSearch(ip);
	        String addr = block.getRegion().replace("|", ";");
	        String[] as = addr.split(";");
	        String ad = as[2]+as[3];
	        map.put("city", ad.replace("0", "").trim());
	        return map;
		} catch (Exception e) {
			log.error("根据IP获取城市异常："+e);
		}
		return map;
	}
	
}
