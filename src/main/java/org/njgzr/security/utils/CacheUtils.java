package org.njgzr.security.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Maps;

/**
*@author Mr Gu[admin@njgzr.org]
*@version Dec 11, 2019 , 10:00:00 AM
*/
@SuppressWarnings("rawtypes")
public class CacheUtils {
	
	public static Map<String, CacheData> cache = Maps.newConcurrentMap();
	
	static {  
		ScheduledExecutorService t = Executors.newScheduledThreadPool(1);  
		t.scheduleWithFixedDelay(new ClearTimerTask(cache), 0, 60 ,TimeUnit.SECONDS);
    }
	
	/**
	 * 缓存大小
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 3, 2020 , 10:28:25 AM
	 * @return
	 */
	public static int getCacheSize(){
		return cache.size();
	}
	
	public static Set<String> getkeys(){
		return cache.keySet();
	}
	
	public static Map<String, CacheData> getCache(){
		return cache;
	}
	
	public static <T> void set(String key, T t) {  
	        cache.put(key, new CacheData<>(t, 0));  
	}  
	
    public static <T> void set(String key, T t, long expire) {  
        cache.put(key, new CacheData<>(t, expire));  
    }  
    
    @SuppressWarnings("unchecked")  
    public static <T> T get(String key) {  
        CacheData<T> data = cache.get(key);  
        if(null == data) {  
            return null;  
        }  
        if(data.isExpire()) {  
            remove(key);  
            return null;  
        }  
        return data.getData();  
    }  
    
    public static Boolean hasKey(String key){
    	return cache.containsKey(key);
    }
    
    public static void remove(String key) {  
        cache.remove(key);  
    }  
    
    public static void removeAll() {  
        cache.clear();  
    }  
    
    private static class CacheData<T> {  
        private T data;  
        private long expireTime;  
        
        public CacheData(T t, long expire) {  
            this.data = t;  
            if(expire <= 0) {  
                this.expireTime = 0L;  
            } else {  
                this.expireTime = Calendar.getInstance().getTimeInMillis() + expire;  
            }  
        }  
          
        public boolean isExpire() {  
            if(expireTime <= 0) {  
                return false;  
            }  
            if(expireTime > Calendar.getInstance().getTimeInMillis()) {  
                return false;  
            }  
            return true;  
        }  
          
        public T getData() {  
            return data;  
        }
        
        @SuppressWarnings("unused")
		public String getExpireTime() {
        	return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(expireTime));
		}
        
        @SuppressWarnings("unused")
		public String getTime() {
        	return ((expireTime - Calendar.getInstance().getTimeInMillis())/1000)+"秒";
        }
        
    }
    
    private static class ClearTimerTask extends TimerTask {  
  
        Map<String, CacheData> cache;  
          
        public ClearTimerTask(Map<String, CacheData> cache) {  
            this.cache = cache;  
        }  
          
        @Override  
        public void run() {  
            Set<String> keys = cache.keySet();  
            for(String key : keys) {  
                CacheData<?> data = cache.get(key);  
                if(data.expireTime <= 0) {  
                    continue;  
                }  
                if(data.expireTime > Calendar.getInstance().getTimeInMillis()) {  
                    continue;  
                }  
                cache.remove(key);  
            }  
        }  
    } 
	
	
	
	
}
