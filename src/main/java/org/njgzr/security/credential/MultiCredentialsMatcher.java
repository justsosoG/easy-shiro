package org.njgzr.security.credential;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;

public class MultiCredentialsMatcher implements CredentialsMatcher{

	private CredentialsMatcher[] matchers = null;
	
	public MultiCredentialsMatcher(CredentialsMatcher... matchers){
		this.matchers = matchers;
	}
	
	@Override
	public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
		for(CredentialsMatcher matcher:matchers){
			if(matcher.doCredentialsMatch(token, info))
				return true;
		}
		return false;
	}

}
