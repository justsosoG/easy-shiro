package org.njgzr.security.enums;
/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 17, 2019 , 11:24:01 AM
*/
public enum CaptchaType {
	
	SpecCaptcha,
	GifCaptcha,
//	ChineseCaptcha,
//	ChineseGifCaptcha,
	ArithmeticCaptcha;
	
}
