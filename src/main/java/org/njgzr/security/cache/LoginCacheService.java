package org.njgzr.security.cache;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.njgzr.security.cache.impl.MemoryCache;
import org.njgzr.security.cache.impl.RedisCache;
import org.njgzr.security.interfaces.ConfigGetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
*@author Mr Gu[admin@njgzr.org]
*@version Dec 11, 2019 , 10:16:59 AM
*/
@Service
public class LoginCacheService {
	
	private LoginCache realCache = null;
	
	public void saveAndKitOutSession(String sessionId,String name, String key,int terminal,String uname) {
		realCache.saveAndKitOutSession(sessionId,name, key, terminal,uname);
	}
	
	public boolean existSession(String key) {
		return realCache.existSession(key);
	}
	
	public void removeSession(String key) {
		realCache.removeSession(key);
	}
	
	public String cachePutIfAbsent(String key,String value) {
		return realCache.cachePutIfAbsent(key, value);
	}
	
	public String cacheGet(String key) {
		return realCache.cacheGet(key);
	}
	
	public String cachePutIfAbsent(String key, String value, long timeOut, TimeUnit uint) {
		return realCache.cachePutIfAbsent(key, value, timeOut, uint);
	}
	
	
	
	
	
	
    private StringRedisTemplate redisTemplate;
    
	@Autowired
    private ConfigGetService configGetService;
	
	@PostConstruct
	public void init(){
		this.redisTemplate = configGetService.getStringRedisTemplate();
		if(redisTemplate!=null)
			realCache = new RedisCache(configGetService,redisTemplate);
		else
			realCache = new MemoryCache(configGetService);
	}
	
	
}
