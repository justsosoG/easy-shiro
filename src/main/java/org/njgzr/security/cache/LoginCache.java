package org.njgzr.security.cache;

import java.util.concurrent.TimeUnit;

/**
*@author Mr Gu[admin@njgzr.org]
*@version Dec 11, 2019 , 10:00:00 AM
*/
public interface LoginCache {
	
	void saveAndKitOutSession(String sessionId,String name, String key,int terminal,String uname);
	
	boolean existSession(String key);
	
	void removeSession(String key);
	
	String cachePutIfAbsent(String key,String value);
	
	String cacheGet(String key);
	
	String cachePutIfAbsent(String key, String value, long timeOut, TimeUnit uint);
	
	void saveLoginSession(Long userId,String loginName,String terminal,String addr,String ip,String os,String browser);
	
}
