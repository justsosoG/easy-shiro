package org.njgzr.security.cache.impl;

import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.digest.DigestUtils;
import org.njgzr.security.base.Contance;
import org.njgzr.security.cache.LoginCache;
import org.njgzr.security.interfaces.ConfigGetService;
import org.njgzr.security.utils.JWTUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
*@author Mr Gu[admin@njgzr.org]
*@version Dec 11, 2019 , 10:07:53 AM
*/
@Slf4j
@Service
public class RedisCache implements LoginCache {
	
	private ConfigGetService configGetService;
	
	private StringRedisTemplate redisTemplate;
    
	public RedisCache(ConfigGetService configGetService,StringRedisTemplate redisTemplate) {
		this.configGetService = configGetService;
		this.redisTemplate = redisTemplate;
	}
    
	long getExpireTime(int terminal) {
		switch (terminal) {
			case 1:
				return configGetService.webExpireTime()==null?Contance.webExpireTime:configGetService.webExpireTime();
			case 2:
				return configGetService.appExpireTime()==null?Contance.appExpireTime:configGetService.appExpireTime();
			case 3:
				return configGetService.pcExpireTime()==null?Contance.pcExpireTime:configGetService.pcExpireTime();
			case 7:
				return 15*24*60*1000;
			default:
				return configGetService.webExpireTime()==null?Contance.webExpireTime:configGetService.webExpireTime();
		}
	}
	
	public void saveSession(String sessionId, String name,int terminal,String token) {
		redisTemplate.opsForValue().set(sessionId, name+"-"+token,getExpireTime(terminal)+Contance.gracePeriod,TimeUnit.MILLISECONDS);
	}
	
	@Override
	public void saveAndKitOutSession(String token,String key1, String value,int terminal,String uname) {
		String sessionId = DigestUtils.md5Hex(token);
		saveSession(sessionId,key1,terminal,token);
		Long size = listSize(key1);
		Long mLong = configGetService.maxSessionCount();
		if(size>=(mLong == null?2:mLong)) {
			String session = redisTemplate.opsForList().rightPop(key1, 30, TimeUnit.SECONDS);
			removeSession(session.split(":")[1].split("-")[0]);
			String key = JWTUtil.getKey(session.split("-")[1]);
			removeSession(buildSaltKey(uname, key, terminal));
		}
		redisTemplate.opsForList().leftPush(key1, sessionKey(sessionId,token));
	}

	private Long listSize(String name) {
		return redisTemplate.opsForList().size(name);
	}
	
	@Override
	public boolean existSession(String key) {
		return redisTemplate.hasKey(key);
	}

	@Override
	public void removeSession(String key) {
		if(redisTemplate.hasKey(key)) {
			redisTemplate.delete(key);
			log.debug("session:"+key+"已被移除");
		}
	}
	
	private String sessionKey(String key,String token) {
		return "session:"+key+"-"+token;
	}
	
	private String buildSaltKey(String username, String key, int teminalType) {
		return "jwt:salt:"+configGetService.getAppId()+":"+username+":"+teminalType+":"+key;
	}

	@Override
	public String cachePutIfAbsent(String key, String value) {
		if(redisTemplate.opsForValue().setIfAbsent(key, value))
			return value;
		return redisTemplate.opsForValue().get(key);
	}

	@Override
	public String cacheGet(String key) {
		return redisTemplate.opsForValue().get(key);
	}

	@Override
	public String cachePutIfAbsent(String key, String value, long timeOut, TimeUnit uint) {
		if(redisTemplate.opsForValue().setIfAbsent(key, value)) {
			redisTemplate.expire(key, timeOut, uint);
			return value;
		}
		return redisTemplate.opsForValue().get(key);
	}

	@Override
	public void saveLoginSession(Long userId, String loginName, String terminal, String addr, String ip, String os,
			String browser) {
		
	}

}
