package org.njgzr.security.cache.impl;

import java.io.Serializable;
import java.util.Deque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.digest.DigestUtils;
import org.njgzr.security.base.Contance;
import org.njgzr.security.cache.LoginCache;
import org.njgzr.security.interfaces.ConfigGetService;
import org.njgzr.security.utils.CacheUtils;
import org.njgzr.security.utils.JWTUtil;

import lombok.extern.slf4j.Slf4j;

/**
*@author Mr Gu[admin@njgzr.org]
*@version Dec 11, 2019 , 10:20:03 AM
*/
@Slf4j
public class MemoryCache implements LoginCache {
	
	long getExpireTime(int terminal) {
		switch (terminal) {
			case 1:
				return configGetService.webExpireTime()==null?Contance.webExpireTime:configGetService.webExpireTime();
			case 2:
				return configGetService.appExpireTime()==null?Contance.appExpireTime:configGetService.appExpireTime();
			case 3:
				return configGetService.pcExpireTime()==null?Contance.pcExpireTime:configGetService.pcExpireTime();
			case 7:
				return 15*24*60*1000;
			default:
				return configGetService.webExpireTime()==null?Contance.webExpireTime:configGetService.webExpireTime();
		}
	}
	
	private ConcurrentHashMap<String, Deque<Serializable>> sessions = new ConcurrentHashMap<String, Deque<Serializable>>();
	
	private ConfigGetService configGetService;
	
	public MemoryCache(ConfigGetService configGetService) {
		this.configGetService = configGetService;
	}
	
	@Override
	public boolean existSession(String key) {
		return CacheUtils.hasKey(key);
	}

	@Override
	public void removeSession(String key) {
		if(CacheUtils.hasKey(key)) {
			CacheUtils.remove(key);
			log.debug("session:"+key+"已被移除");
		}
		
	}
	
	public void saveSession(String sessionId, String name,int terminal,String token) {
		CacheUtils.set(sessionId, name+"-"+token, getExpireTime(terminal)+Contance.gracePeriod);
	}
	
	@Override
	public void saveAndKitOutSession(String token,String key1, String value,int terminal,String uname) {
		String sessionId = DigestUtils.md5Hex(token);
		saveSession(sessionId,key1,terminal,token);
		Integer size = listSize(key1);
		Long mLong = configGetService.maxSessionCount();
		if(size>=(mLong == null?2:mLong)) {
			String session = getKeyLast(key1);
			removeSession(session.split(":")[1].split("-")[0]);
			String key = JWTUtil.getKey(session.split("-")[1]);
			removeSession(buildSaltKey(uname, key, terminal));
		}
		if(!sessions.containsKey(key1)) {
			Deque<Serializable> de = new LinkedBlockingDeque<Serializable>();
			de.addFirst(sessionKey(sessionId,token));
			sessions.putIfAbsent(key1, de);
		}else {
			Deque<Serializable> de = sessions.get(key1);
			de.addFirst(sessionKey(sessionId,token));
			sessions.putIfAbsent(key1, de);
		}
	}
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:19:48 PM
	 * @param key key
	 * @return 该用户已经登陆的数量
	 */
	private Integer listSize(String key){
		Deque<Serializable> de = sessions.get(key);
		if(de!=null) {
			return de.size();
		}
		return 0;
	}
	
	/**
	 * 取出第一次登陆的sessionId
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Jan 2, 2020 , 5:19:05 PM
	 * @param key key
	 * @return String
	 */
	private String getKeyLast(String key) {
		Deque<Serializable> de = sessions.get(key);
		if(de!=null) {
			return de.pollLast().toString();
		}
		return null;
	}
	
	private String sessionKey(String key,String token) {
		return "session:"+key+"-"+token;
	}
	
	private String buildSaltKey(String username, String key, int teminalType) {
		return "jwt:salt:"+configGetService.getAppId()+":"+username+":"+teminalType+":"+key;
	}
	
	@Override
	public String cachePutIfAbsent(String key, String value) {
		if(!CacheUtils.hasKey(key)) {
			CacheUtils.set(key, value);
			return value;
		}
		return CacheUtils.get(key);
	}
	
	@Override
	public String cacheGet(String key) {
		return CacheUtils.get(key);
	}

	@Override
	public String cachePutIfAbsent(String key, String value, long timeOut, TimeUnit uint) {
		if(!CacheUtils.hasKey(key)) {
			CacheUtils.set(key, value,timeOut);
			return value;
		}
		return CacheUtils.get(key);
	}

	@Override
	public void saveLoginSession(Long userId, String loginName, String terminal, String addr, String ip, String os,
			String browser) {
		
	}
	
}
