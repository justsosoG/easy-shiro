package org.njgzr.security.controller;

import org.njgzr.security.base.Result;
import org.njgzr.security.utils.CacheUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/easy")
public class EasyController {
	
	@RequestMapping(value="/cache")
	public Result cache(){
		return Result.success(CacheUtils.getCache());
	}
	
}
