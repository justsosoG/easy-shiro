package org.njgzr.security;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.ModularRealmAuthorizer;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.PrincipalCollection;

/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 13, 2019 , 2:00:08 PM
*/
public class MyModularRealmAuthorizer extends ModularRealmAuthorizer {

	@Override
	public void checkPermission(PrincipalCollection principals, String permission) throws AuthorizationException {
		super.assertRealmsConfigured();
        if (!super.isPermitted(principals, permission)) {
            throw new UnauthorizedException("您的账号没有["+permission+"]权限！");
        }
	}

	@Override
	public void checkRole(PrincipalCollection principals, String role) throws AuthorizationException {
		super.assertRealmsConfigured();
        if (!super.hasRole(principals, role)) {
            throw new UnauthorizedException("您的账号没有 ["+role+"] 角色！");
        }
	}
	
	
	
}
