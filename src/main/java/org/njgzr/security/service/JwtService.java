package org.njgzr.security.service;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 11, 2019 , 5:27:34 PM
 * Description
 */
public interface JwtService {
	
	String createToken(String username,int teminalType);
	
	String refreshToken(String token, String username);
	
	boolean validateToken(String token,String username);
	
	boolean shouldRefreshToken(String token);
	
	void clearToken(String token);
	
}
