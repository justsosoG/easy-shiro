package org.njgzr.security.service;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.njgzr.security.base.Contance;
import org.njgzr.security.cache.LoginCacheService;
import org.njgzr.security.interfaces.ConfigGetService;
import org.njgzr.security.utils.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 11, 2019 , 5:27:42 PM
 * Description
 */
@Service
public class JwtServiceImpl implements JwtService {
	
	long getExpireTime(int terminal) {
		switch (terminal) {
			case 1:
				return configGetService.webExpireTime()==null?Contance.webExpireTime:configGetService.webExpireTime();
			case 2:
				return configGetService.appExpireTime()==null?Contance.appExpireTime:configGetService.appExpireTime();
			case 3:
				return configGetService.pcExpireTime()==null?Contance.pcExpireTime:configGetService.pcExpireTime();
			case 7:
				return 15*24*60*1000;
			default:
				return configGetService.webExpireTime()==null?Contance.webExpireTime:configGetService.webExpireTime();
		}
	}
	
	@Override
	public String createToken(String username,int teminalType) {
		String key = UUID.randomUUID().toString();
		String salt = cacheService.cachePutIfAbsent(buildSaltKey(username, key, teminalType), UUID.randomUUID().toString());
		String token = JWTUtil.sign(username, salt, key, getExpireTime(teminalType)+Contance.gracePeriod,teminalType);
		return token;
	}
	
	@Override
	public String refreshToken(String token,String username) {
		String key = JWTUtil.getKey(token);
		int terminal = JWTUtil.getTerminal(token);
		String salt = cacheService.cacheGet(buildSaltKey(username,key ,terminal));
		if(salt==null)
			return null;

		String newToken = JWTUtil.sign(username, salt, key, getExpireTime(terminal)+Contance.gracePeriod,terminal);
		
		return cacheService.cachePutIfAbsent(tokenGraceCacheKey(token),newToken,Contance.gracePeriod,TimeUnit.MILLISECONDS);		
	}
	
	@Override
	public boolean validateToken(String token, String username) {
		String salt = cacheService.cacheGet(buildSaltKey(username,JWTUtil.getKey(token), JWTUtil.getTerminal(token)));
		return JWTUtil.verify(token, username, salt);
	}

	@Override
	public boolean shouldRefreshToken(String token) {
		Date issueTime = JWTUtil.getIssuedAt(token);
		int terminal = JWTUtil.getTerminal(token);
		return issueTime.getTime()+getExpireTime(terminal)<=System.currentTimeMillis()
				&&issueTime.getTime()+getExpireTime(terminal)+Contance.gracePeriod>System.currentTimeMillis();
	}
	
	@Override
	public void clearToken(String token) {
		cacheService.removeSession(buildSaltKey(JWTUtil.getUsername(token),JWTUtil.getKey(token), JWTUtil.getTerminal(token)));
	}
	
	private String buildSaltKey(String username, String key, int teminalType) {
		return "jwt:salt:"+appId+":"+username+":"+teminalType+":"+key;
	}
	
	private String tokenGraceCacheKey(String token) {
		return "jwt:token:grace:"+token;
	}
	
	private String appId;
	
	@PostConstruct
	public void getAppId() {
		this.appId = configGetService.getAppId();
	}
	
	@Autowired
	private ConfigGetService configGetService;
	
	@Autowired
	private LoginCacheService cacheService;
	
}
