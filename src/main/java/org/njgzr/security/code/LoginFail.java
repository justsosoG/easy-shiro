package org.njgzr.security.code;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;

import com.google.common.collect.Lists;

import lombok.Synchronized;

public class LoginFail {

	private Integer expireTime;
	
	
	/**
	 * login fail time
	 */
	private List<Date> loginTimes = Lists.newArrayList();
	
	/**
	 * last login fail time
	 */
	private Date lastTime = null;
	
	public LoginFail(){
		this(60);
	}
	
	public LoginFail(Integer expireTime){
		this.expireTime=expireTime;
	}
	
	@Synchronized
	public LoginFail count(Date time) {
		
		this.loginTimes.add(time);
		if(this.lastTime == null|| this.lastTime.before(new Date())){
			this.lastTime = time;
		}
		return this;
	}
	@Synchronized
	public int size() {
		if(this.lastTime==null||this.loginTimes.isEmpty()){
			return 0;
		}
		
		Date checkTime = DateUtils.addMinutes(this.lastTime, this.expireTime);//当前时间的一小时前
		if(checkTime.before(new Date())){
			this.loginTimes.clear();
			this.lastTime = null;
			return 0;
		}
		return this.loginTimes.size();
	}
	
}
