package org.njgzr.security.code;
/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 16, 2019 , 11:32:38 AM
*/
public interface UserLoginCounter {
	
	void loginSuccess(String principal);
	
	void loginFail(String principal);
	
	int getUserLoginFailCount(String principal);
	
}
