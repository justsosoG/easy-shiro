package org.njgzr.security.code;

import org.njgzr.security.event.LoginFailEvent;
import org.njgzr.security.event.LoginSuccessEvent;
import org.njgzr.security.interfaces.ConfigGetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import lombok.extern.slf4j.Slf4j;

/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 16, 2019 , 11:16:51 AM
*/
@Service
@Slf4j
public class CodeService {
	
	public boolean isCaptchaEnabled(@RequestBody String principal){
		if(userLoginCounter == null)
			return true;
		if(configGetService.maxClfCount()==0)
			return true;
		return userLoginCounter.getUserLoginFailCount(principal) >= configGetService.maxClfCount() ;
	}
	@EventListener({LoginSuccessEvent.class})
	public void loginSuccess(LoginSuccessEvent event){
		if(userLoginCounter!=null)
			userLoginCounter.loginSuccess(event.getUser().getLoginName());
	}
	@EventListener({LoginFailEvent.class})
	public void loginFail(LoginFailEvent event){
		try {
			if(userLoginCounter!=null)
				userLoginCounter.loginFail(event.getLoginName());
		} catch (Exception e) {
			log.error("",e);
		}
	}

	public void loginFail(String principal) {
		if(userLoginCounter!=null)
			userLoginCounter.loginFail(principal);
	}
	
	@Autowired
	private UserLoginCounter userLoginCounter;
	
	@Autowired
	private ConfigGetService configGetService;
	
}
