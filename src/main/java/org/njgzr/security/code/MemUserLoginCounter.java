package org.njgzr.security.code;

import java.util.Date;
import java.util.concurrent.ConcurrentMap;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

@Service
public class MemUserLoginCounter implements UserLoginCounter{
	
	private Integer expireTime;
	
	public MemUserLoginCounter(){
		this(60);
	}
	
	public MemUserLoginCounter(Integer expireTime){
		this.expireTime=expireTime;
	}
	
	private ConcurrentMap<String, LoginFail> userLoginFailLog = Maps.newConcurrentMap();
	
	@Override
	public void loginFail(String principal) {
		userLoginFailLog.putIfAbsent(principal, new LoginFail(this.expireTime));
		userLoginFailLog.get(principal).count(new Date());
	}
	
	@Override
	public void loginSuccess(String principal) {
		userLoginFailLog.remove(principal);
	}

	@Override
	public int getUserLoginFailCount(String principal) {
		LoginFail loginFail = userLoginFailLog.getOrDefault(principal, new LoginFail(this.expireTime));
		return  loginFail.size();
	}
 	
}
