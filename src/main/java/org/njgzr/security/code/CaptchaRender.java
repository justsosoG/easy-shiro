package org.njgzr.security.code;

import java.awt.FontFormatException;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.njgzr.security.interfaces.ConfigGetService;
import org.njgzr.security.utils.CacheUtils;
import org.njgzr.security.utils.IpUtil;

import com.google.common.collect.Lists;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.ChineseCaptcha;
import com.wf.captcha.ChineseGifCaptcha;
import com.wf.captcha.GifCaptcha;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;

import lombok.extern.slf4j.Slf4j;

/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 17, 2019 , 10:38:40 AM
*/
@Slf4j
public class CaptchaRender {
	
	public Integer width;
	
	public Integer height;
	
	public Integer length;
	
	public void render(HttpServletRequest request, HttpServletResponse response,ConfigGetService configGetService) {
		try {
			// 设置请求头为输出图片类型
			response.setContentType("image/gif");
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			
			Captcha captcha = specCaptcha(request,configGetService.captchaSize(),configGetService.captchaLenth());
			String typeString = StringUtils.isBlank(configGetService.captchaType())?
					randomFromList(Lists.newArrayList("specCaptcha","gifCaptcha","arithmeticCaptcha"))
					:configGetService.captchaType();
			switch (typeString) {
			case "specCaptcha":
				break;
			case "gifCaptcha":
				captcha = gifCaptcha(request,configGetService.captchaSize(),configGetService.captchaLenth());
				break;
			case "chineseCaptcha":
				captcha = chineseCaptcha(request,configGetService.captchaSize(),configGetService.captchaLenth());
				break;
			case "chineseGifCaptcha":
				captcha = chineseGifCaptcha(request,configGetService.captchaSize(),configGetService.captchaLenth());
				break;
			case "arithmeticCaptcha":
				captcha = arithmeticCaptcha(request,configGetService.captchaSize());
				break;
			default:
				break;
			}
			
			captcha.out(response.getOutputStream());
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				e.printStackTrace();
				return;
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	/**
	 * SpecCaptcha
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 17, 2019 , 10:53:48 AM
	 * @param request
	 * @throws FontFormatException 
	 * @throws IOException 
	 */
	private Captcha specCaptcha(HttpServletRequest request,String size,int len) throws IOException, FontFormatException {
		log.debug("specCaptcha");
		SpecCaptcha captcha = new SpecCaptcha(getWidth(size), getHeight(size),getLen(len));
        captchaStyle(request, captcha);
     	return captcha;
	}
	
	/**
	 * gifCaptcha
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 17, 2019 , 10:55:58 AM
	 * @param request request
	 * @return Captcha
	 * @throws FontFormatException 
	 * @throws IOException 
	 */
	private Captcha gifCaptcha(HttpServletRequest request,String size,int len) throws IOException, FontFormatException {
		log.debug("gifCaptcha");
		GifCaptcha  captcha = new GifCaptcha(getWidth(size), getHeight(size),getLen(len));
		captchaStyle(request, captcha);
     	return captcha;
	}
	
	/**
	 * chineseCaptcha 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 17, 2019 , 10:56:34 AM
	 * @param request
	 * @return
	 * @throws FontFormatException 
	 * @throws IOException 
	 */
	private Captcha chineseCaptcha(HttpServletRequest request,String size,int len) throws IOException, FontFormatException {
		log.debug("chineseCaptcha");
		ChineseCaptcha captcha = new ChineseCaptcha(getWidth(size), getHeight(size),getLen(len));
		captchaStyle(request, captcha);
		return captcha;
	}
	
	private int getWidth(String size) {
		if(StringUtils.isBlank(size)) {
			return 130;
		}
		int width;
		try {
			width = Integer.valueOf(size.split(",")[0]);
		} catch (Exception e) {
			width = 130;
		}
		return width;
	}
	
	private int getLen(int len) {
		if(len<4) {
			return 4;
		}else if(len>6){
			return 6;
		}else {
			return len;
		}
	}
	
	private int getHeight(String size) {
		if(StringUtils.isBlank(size)) {
			return 48;
		}
		int height;
		try {
			height = Integer.valueOf(size.split(",")[1]);
		} catch (Exception e) {
			height = 48;
		}
		return height;
	}
	
	/**
	 * chineseGifCaptcha
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 17, 2019 , 10:57:29 AM
	 * @param request
	 * @return
	 * @throws FontFormatException 
	 * @throws IOException 
	 */
	private Captcha chineseGifCaptcha(HttpServletRequest request,String size,int len) throws IOException, FontFormatException {
		log.debug("chineseGifCaptcha");
		ChineseGifCaptcha captcha = new ChineseGifCaptcha(getWidth(size), getHeight(size),getLen(len));
		captchaStyle(request, captcha);
		return captcha;
	}

	private void captchaStyle(HttpServletRequest request, Captcha captcha) throws IOException, FontFormatException {
		// 设置字体
//		captcha.setFont(new Font("Verdana", Font.PLAIN, 32));  // 有默认字体，可以不用设置
		captcha.setFont(randomFromList(Lists.newArrayList(1,2,3,4,5,6,7,8,9,0)),32f);
		// 设置类型，纯数字、纯字母、字母数字混合
//		captcha.setCharType(Captcha.TYPE_ONLY_NUMBER);
		
		// 验证码存入session
//		request.getSession().setAttribute("captcha", captcha.text().toLowerCase());
		CacheUtils.set("captcha_"+getUserName(request), captcha.text().toLowerCase(),5*60*1000);
	}
	
	/**
	 * 
	 * @author: Mr Gu [admin@njgzr.org]
	 * @version Dec 17, 2019 , 10:58:45 AM
	 * @param request
	 * @return
	 * @throws FontFormatException 
	 * @throws IOException 
	 */
	private Captcha arithmeticCaptcha(HttpServletRequest request,String size) throws IOException, FontFormatException {
		// 算术类型
		log.debug("arithmeticCaptcha");
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(getWidth(size), getHeight(size));
        captcha.getArithmeticString();// 获取运算的公式：3+2=?
        captchaStyle(request, captcha);
		return captcha;
	}
	
	public boolean validate2(String inputverCode,HttpServletRequest request) {
		try {
			// 获取session中的验证码
//			Object object = request.getSession().getAttribute("captcha");
			String key = "captcha_"+getUserName(request);
			Object object = CacheUtils.get(key);
			CacheUtils.remove(key);
			if(object!=null) {
				String sessionCode = object.toString();
				// 判断验证码
				if (inputverCode !=null && sessionCode.equals(inputverCode.trim().toLowerCase())) {
					return true;
				}
			}
		} catch (Exception e) {
			log.error("validate captcha code error:",e);
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T randomFromList(List<?> list) {
		return (T) list.get((int) (Math.random() * list.size()));
	}
	
	
	public String getUserName(HttpServletRequest request) throws IOException {
    	return IpUtil.getIpAddr(request);
	}
	
}
