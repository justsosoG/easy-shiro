package org.njgzr.security.event;

import javax.servlet.http.HttpServletRequest;

import org.njgzr.security.base.AuthorizedUser;
import org.springframework.context.ApplicationEvent;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 11, 2019 , 5:34:03 PM
 * Description
 */
@Getter @Setter @ToString
public class LoginSuccessEvent extends ApplicationEvent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7897287570536260847L;
	
	private String terminal;
	private HttpServletRequest http;
	private String jwtToken;
	private Boolean refresh;
	
	public LoginSuccessEvent(AuthorizedUser user,HttpServletRequest http,String jwtToken,Boolean refresh) {
		super(user);
		this.http = http;
		this.jwtToken = jwtToken;
		this.refresh = refresh;
	}

	public AuthorizedUser getUser(){
		return (AuthorizedUser)getSource();
	}
	
	public HttpServletRequest getHttp() {
		return http;
	}
	
	public String getToken() {
		return jwtToken;
	}
	public Boolean getRefresh() {
		return refresh;
	}
}
