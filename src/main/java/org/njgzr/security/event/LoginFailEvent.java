package org.njgzr.security.event;

import org.springframework.context.ApplicationEvent;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 9, 2019 , 3:42:09 PM
 * Description
 */
@Getter @Setter @ToString
public class LoginFailEvent extends ApplicationEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1513785696778314381L;
	
	private Exception e;
	
	public LoginFailEvent(String loginName, Exception e) {
		super(loginName);
		this.e = e;
	}

	public String getLoginName(){
		return (String)getSource();
	}
	public Exception getException() {
		return e;
	}
	
	
}
