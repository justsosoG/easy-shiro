package org.njgzr.security.event;

import org.njgzr.security.base.AuthorizedUser;
import org.springframework.context.ApplicationEvent;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Mr Gu[admin@njgzr.org]
 * @version Dec 11, 2019 , 5:34:03 PM
 * Description
 */
@Getter @Setter @ToString
public class LogoutEvent extends ApplicationEvent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7897287570536260847L;
	
	public LogoutEvent(AuthorizedUser user) {
		super(user);
	}

	public AuthorizedUser getUser(){
		return (AuthorizedUser)getSource();
	}
}
