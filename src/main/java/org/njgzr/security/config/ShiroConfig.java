package org.njgzr.security.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.authz.ModularRealmAuthorizer;
import org.apache.shiro.mgt.DefaultSessionStorageEvaluator;
import org.apache.shiro.mgt.DefaultSubjectDAO;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.njgzr.security.MyModularRealmAuthenticator;
import org.njgzr.security.MyModularRealmAuthorizer;
import org.njgzr.security.base.filter.AjaxAuthenticationFilter;
import org.njgzr.security.base.filter.AjaxLogoutFilter;
import org.njgzr.security.base.filter.CodeFilter;
import org.njgzr.security.base.filter.JWTFilter;
import org.njgzr.security.base.realm.DbRealm;
import org.njgzr.security.base.realm.JWTRealm;
import org.njgzr.security.cache.LoginCacheService;
import org.njgzr.security.code.CodeService;
import org.njgzr.security.interfaces.ConfigGetService;
import org.njgzr.security.service.JwtServiceImpl;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

/**
 * 
 * @author Mr Gu [admin@njgzr.org]
 * @version Dec 11, 2019 , 3:38:47 PM
 * Description
 */
@Configuration
public class ShiroConfig {
	
	
	@Bean
    public FilterRegistrationBean<CodeFilter> jcaptchaFilterRegistrationBean(ConfigGetService configGetService) {  
        FilterRegistrationBean<CodeFilter> filterRegistration = new FilterRegistrationBean<CodeFilter>();  
        filterRegistration.setFilter(new CodeFilter(configGetService));   
        filterRegistration.setEnabled(true);  
        filterRegistration.addUrlPatterns("/captcha");   
        filterRegistration.setDispatcherTypes(DispatcherType.REQUEST);  
        filterRegistration.setOrder(2);
        return filterRegistration;  
    }
	
	@Bean("securityManager")
    public DefaultWebSecurityManager defaultWebSecurityManager(JWTRealm userRealm,DbRealm dbRealm) {
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        
        defaultWebSecurityManager.setAuthenticator(modularRealmAuthenticator());
        defaultWebSecurityManager.setAuthorizer(modularRealmAuthorizer());
        
        List<Realm> realms = new ArrayList<>();
        realms.add(userRealm);
        realms.add(dbRealm);
        defaultWebSecurityManager.setRealms(realms);
        defaultWebSecurityManager.setSubjectDAO(getDefaultSubjectDAO());
        
        return defaultWebSecurityManager;
    }
    
    private DefaultSubjectDAO getDefaultSubjectDAO() {
    	DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
        DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
        defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
        subjectDAO.setSessionStorageEvaluator(defaultSessionStorageEvaluator);
        return subjectDAO;
    }
    
    @Bean
	public ModularRealmAuthenticator modularRealmAuthenticator() {
		MyModularRealmAuthenticator modularRealmAuthenticator = new MyModularRealmAuthenticator();
	    modularRealmAuthenticator.setAuthenticationStrategy(new AtLeastOneSuccessfulStrategy());
	    return modularRealmAuthenticator;
	}
    
    @Bean
    public ModularRealmAuthorizer modularRealmAuthorizer() {
    	MyModularRealmAuthorizer modularRealmAuthorizer = new MyModularRealmAuthorizer();
    	return modularRealmAuthorizer;
    }
    
    @Bean("shiroFilterFactoryBean")
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager,
    		JwtServiceImpl jwtService,LoginCacheService loginCache,
    		ApplicationContext applicationContext,ConfigGetService configGetService,CodeService codeService) {
        ShiroFilterFactoryBean factoryBean = new ShiroFilterFactoryBean();
        Map<String, Filter> filterMap = new HashMap<>(16);
        filterMap.put("jwt", new JWTFilter(jwtService,loginCache,applicationContext,configGetService));
        filterMap.put("auth", new AjaxAuthenticationFilter(jwtService,loginCache,applicationContext,configGetService,codeService));
        filterMap.put("logout", new AjaxLogoutFilter(jwtService,loginCache,applicationContext,configGetService));
        factoryBean.setFilters(filterMap);
        factoryBean.setSecurityManager(securityManager);
        String loginUrl = configGetService.loginUrl();
        String url = StringUtils.isBlank(loginUrl)?"/login":loginUrl;
        factoryBean.setLoginUrl(url);
        LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>(16);
        List<String> as = configGetService.anons();
        if(as!=null) {
        	for(String str : as) {
        		filterChainDefinitionMap.put(str, "noSessionCreation,anon");
        	}
        }
        filterChainDefinitionMap.put("/easy/cache", "noSessionCreation,anon");
        filterChainDefinitionMap.put(url, "noSessionCreation,auth");
        filterChainDefinitionMap.put("/logout", "noSessionCreation,logout");
        
        filterChainDefinitionMap.put("/**", "noSessionCreation,jwt");
        factoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return factoryBean;
    }
    
    @Bean
    @DependsOn("lifecycleBeanPostProcessor")
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
        return defaultAdvisorAutoProxyCreator;
    }

    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }
    
}
