package org.njgzr.security.rateLimit;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.njgzr.security.base.Result;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.util.concurrent.RateLimiter;

import lombok.extern.slf4j.Slf4j;

/**
*@author Mr Gu [admin@njgzr.org]
*@version Dec 19, 2019 , 11:31:35 AM
*/
@Aspect
@Component
@Slf4j
public class LxRateLimitAspect {
	
//	@Autowired
//	private ConfigGetService configGetService;
	
//	private StringRedisTemplate redisTemplate;
	
	/*
	 * @PostConstruct public void init() { this.redisTemplate =
	 * configGetService.getStringRedisTemplate(); }
	 */
	
	private RateLimiter rateLimiter = RateLimiter.create(Double.MAX_VALUE);
 
    @Pointcut("@annotation(org.njgzr.security.rateLimit.LxRateLimit)")
    public void checkPointcut() { }
 
    @ResponseBody
    @Around(value = "checkPointcut()")
    public Object aroundNotice(ProceedingJoinPoint pjp) throws Throwable {
        log.debug("拦截到了{}方法...", pjp.getSignature().getName());
        Signature signature = pjp.getSignature();
        MethodSignature methodSignature = (MethodSignature)signature;
        //获取目标方法
        Method targetMethod = methodSignature.getMethod();
        if (targetMethod.isAnnotationPresent(LxRateLimit.class)) {
            //获取目标方法的@LxRateLimit注解
            LxRateLimit lxRateLimit = targetMethod.getAnnotation(LxRateLimit.class);
            rateLimiter.setRate(lxRateLimit.perSecond());
            if (!rateLimiter.tryAcquire(lxRateLimit.timeOut(), lxRateLimit.timeOutUnit()))
                return Result.fail("500", "请求过快！");
        }
        return pjp.proceed();
    }
    
//    private void count(String methodName) {
//    	int count = 0 ;
//    	redisTemplate.
//    }
	
}
